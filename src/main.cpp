/*
 * Copyright (C) 2019-2021 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <memory>
#include "gettext.hpp"
#include "arg.hpp"
#include "log.hpp"
#include "msg.hpp"
#include "exception.hpp"

extern "C"
{
#include <unistd.h>
#include <libintl.h>
#include <locale.h>
}

namespace Osa
{
    enum ReturnCode
    {
        SUCCESS,
        INVALID_ARG,
        DAEMON_FAIL,
        FILE_OPEN_FAIL,
        FILE_READ_FAIL,
        FILE_WRITE_FAIL,
        FILE_CLOSE_FAIL
    };
}


int main(int argc, char *argv[])
{
    // Set up initialization.
    setlocale(LC_ALL, "");
    bindtextdomain(OSA_GETTEXT_TEXT_DOMAIN, OSA_GETTEXT_TEXT_DOMAIN_DIR);
    textdomain(OSA_GETTEXT_TEXT_DOMAIN);

    // Parse command-line arguments.
    std::unique_ptr<Osa::Arg> arg_parser(std::make_unique<Osa::Arg>());
    try
    {
        arg_parser->parse_args(argc, argv);
    }
    catch (const Osa::Exception::InvalidArg& error)
    {
        Osa::Msg::error(error.what());
        return Osa::ReturnCode::INVALID_ARG;
    }

    if (std::get<bool>(arg_parser->get_holder().at("help")))
    {
        Osa::Msg::help();
        return Osa::ReturnCode::SUCCESS;
    }

    // Daemonize the process, if requested.
    if (std::get<bool>(arg_parser->get_holder().at("daemonize")))
    {
        try
        {
            if (daemon(1, 0) == -1)
                throw Osa::Exception::DaemonFail();
        }
        catch (const Osa::Exception::DaemonFail& error)
        {
            Osa::Msg::error(error.what());
            return Osa::ReturnCode::DAEMON_FAIL;
        }
    }

    try
    {
        // Record keypresses.
        std::unique_ptr<Osa::Log> log(std::make_unique<Osa::Log>(std::get<std::filesystem::path>(arg_parser->get_holder().at("keyboard")), std::get<std::filesystem::path>(arg_parser->get_holder().at("log"))));
        log->log_keys(std::get<bool>(arg_parser->get_holder().at("newline")));
    }
    catch (const Osa::Exception::FileOpenFail& error)
    {
        Osa::Msg::error(error.what());
        return Osa::ReturnCode::FILE_OPEN_FAIL;
    }
    catch (const Osa::Exception::FileReadFail& error)
    {
        Osa::Msg::error(error.what());
        return Osa::ReturnCode::FILE_READ_FAIL;
    }
    catch (const Osa::Exception::FileWriteFail& error)
    {
        Osa::Msg::error(error.what());
        return Osa::ReturnCode::FILE_WRITE_FAIL;
    }
    catch (const Osa::Exception::FileCloseFail& error)
    {
        Osa::Msg::error(error.what());
        return Osa::ReturnCode::FILE_CLOSE_FAIL;
    }

    return Osa::ReturnCode::SUCCESS;
}
