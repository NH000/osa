/*
 * Copyright (C) 2020-2021 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include "msg.hpp"
#include "info.hpp"
#include "gettext.hpp"

void Osa::Msg::help()
{
    // Find the longest help string to determine necessary indentation.
    // Print help menu.
    std::cout << OSA_INFO_NAME " - " << OSA_INFO_DESC << std::endl;
    std::cout << _("Version") << ": " OSA_INFO_VERS << std::endl;
    std::cout << _("License") << ": " OSA_INFO_LICE << std::endl << std::endl;
    std::cout << _("USAGE") << ": " << std::endl;
    std::cout << "\tosa [-h] [-l " << _("FILE") << "] [-d] [-n] " << _("KEYBOARD") << std::endl << std::endl;
    std::cout << _("OPTIONS") << ":" << std::endl;
    std::cout << "\t-h, --help\n\t\t" << _("display help menu") << std::endl;
    std::cout << "\t-l " << _("FILE") << ", --log " << _("FILE") << "\n\t\t" << _("specify log file") << std::endl;
    std::cout << "\t-d, --daemonize\n\t\t" << _("daemonize the process") << std::endl;
    std::cout << "\t-n, --newline\n\t\t" << _("add final newline to the log file") << std::endl << std::endl;
    std::cout << _("POSITIONAL ARGUMENTS") << ":" << std::endl;
    std::cout << "\t" << _("KEYBOARD") << "\n\t\t" << _("file to read keypresses from") << std::endl;
}
