/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <csignal>
#include "signal.hpp"

namespace Osa
{
    namespace Signal
    {
        static void set(int sig);
        static int current_signal = -1;
    }
}

static void Osa::Signal::set(int sig)
{
    Osa::Signal::current_signal = sig;
}

void Osa::Signal::bind(int sig)
{
    signal(sig, &Osa::Signal::set);
}

void Osa::Signal::unbind(int sig)
{
    signal(sig, SIG_DFL);
}

int Osa::Signal::get()
{
    int current_signal_copy = Osa::Signal::current_signal;
    Osa::Signal::current_signal = -1;
    return current_signal_copy;
}
