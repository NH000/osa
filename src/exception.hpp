/*
 * Copyright (C) 2020-2021 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OSA_EXCEPTION_HPP
#define OSA_EXCEPTION_HPP
#include <exception>
#include <string>

namespace Osa
{
    namespace Exception
    {
        class InvalidArg : public std::exception
        {
            public:
                InvalidArg(const std::string& msg);
                const char *what() const noexcept override;

            private:
                std::string msg;
        };

        class DaemonFail : public std::exception
        {
            public:
                const char *what() const noexcept override;
        };

        class FileOpenFail : public std::exception
        {
            public:
                explicit FileOpenFail(const std::string& path, bool read, bool write);
                const char *what() const noexcept override;

            private:
                std::string msg;
        };

        class FileReadFail : public std::exception
        {
            public:
                explicit FileReadFail(const std::string& path);
                const char *what() const noexcept override;

            private:
                std::string msg;
        };

        class FileWriteFail : public std::exception
        {
            public:
                explicit FileWriteFail(const std::string& path);
                const char *what() const noexcept override;

            private:
                std::string msg;
        };

        class FileCloseFail : public std::exception
        {
            public:
                explicit FileCloseFail(const std::string& path);
                const char *what() const noexcept override;

            private:
                std::string msg;
        };
    }
}

#endif
