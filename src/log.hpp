/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <array>
#include <utility>

namespace Osa
{
    class Log
    {
        public:
            Log(const std::filesystem::path& keyboard_filename, const std::filesystem::path& log_filename); // Opens the keyboard and log file.
            ~Log() noexcept(false);
            void log_keys(bool newline) const;

        private:
            struct Keys
            {
                Keys();

                constexpr static unsigned int no = 249;

                const std::string unknown;
                const std::array<std::string, no> unmodified;
                const std::array<std::string, no> shifted;
            };

            // Files.
            std::pair<std::filesystem::path, int> file_keyboard;    // Keyboard device file (input).
            std::pair<std::filesystem::path, int> file_log;         // Log text file (output).

            // Key maps.
            Keys keys;

            // Helper functions.
            inline bool is_shift(uint16_t key) const noexcept;
            inline const std::string& get_key_name(uint16_t key, uint8_t shift_pressed) const;
    };
}
