/*
 * Copyright (C) 2019-2021 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "arg.hpp"
#include "exception.hpp"
#include "info.hpp"
#include "gettext.hpp"

extern "C"
{
#include <getopt.h>
}

// Initializes holder.
Osa::Arg::Arg()
    : holder{
    {"help", false},
    {"daemonize", false},
    {"keyboard", ""},
    {"log", OSA_INFO_NAME ".log"},
    {"newline", false}}
{
}

// Parses the command-line arguments. Returns success code.
void Osa::Arg::parse_args(const int& argc, char * const *argv)
{
    // Reset parsing position so that parsing starts from the beginning of argv.
    optind = 1;

    // Long option definitions.
    struct option longopts[] =  {
                                    {"help",       no_argument,        NULL,   'h'},
                                    {"daemonize",  no_argument,        NULL,   'd'},
                                    {"log",        required_argument,  NULL,   'l'},
                                    {"newline",    no_argument,        NULL,   'n'},
                                    {0,            0,                  0,      0  }
                                };

    // Parse command-line arguments and populate holder.
    int getopt_ret;
    while ((getopt_ret = getopt_long(argc, argv, ":hdl:n", longopts, NULL)) != -1)  // Error reporting is also disabled.
    {
        switch (getopt_ret)
        {
            case 'h':
                holder.at("help") = true;
                break;
            case 'd':
                holder.at("daemonize") = true;
                break;
            case 'l':
                holder.at("log") = optarg;
                break;
            case 'n':
                holder.at("newline") = true;
                break;
            case '?':
                throw Osa::Exception::InvalidArg(optopt != 0 ? std::string(_("Unrecognized option")) + " -- '" + static_cast<char>(optopt) + "'" : _("Unrecognized long option"));
            case ':':
                throw Osa::Exception::InvalidArg(std::string(_("Option requires a value")) + " -- '" + static_cast<char>(optopt) + "'");
        }
    }

    // Get keyboard.
    if (optind < argc)
    {
        holder.at("keyboard") = argv[optind++];

        // Too many keyboard specified.
        if (optind < argc)
            throw Osa::Exception::InvalidArg(_("Too many keyboards specified"));
    }
    else if (!std::get<bool>(holder.at("help")))
        throw Osa::Exception::InvalidArg(_("No keyboard specified"));
}

const Osa::Arg::Holder& Osa::Arg::get_holder() const noexcept
{
    return holder;
}
