/*
 * Copyright (C) 2019-2021 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <variant>
#include <unordered_map>
#include <string>
#include <filesystem>

// Holds values for the command-line arguments.

namespace Osa
{
    class Arg
    {
        public:
            using Holder = std::unordered_map<std::string, std::variant<bool, std::filesystem::path>>;

            Arg();  // Sets arguments in arg_holder to default values.

            void parse_args(const int& argc, char * const *argv);     // Parses the command-line arguments and stores the parsed information into arg_holder object.
            const Holder& get_holder() const noexcept;                // Returns copy of the argument holder.

        protected:
            Holder holder;  // Argument holder.
    };
}
