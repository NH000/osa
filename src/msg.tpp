/*
 * Copyright (C) 2020-2021 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "info.hpp"
#include "gettext.hpp"

template <typename... T, typename>
void Osa::Msg::error(T&&... msgs)
{
    std::cerr << OSA_INFO_NAME " " << _("error") << ": ";
    (std::cerr << ... << std::forward<T>(msgs)) << std::endl;
}
