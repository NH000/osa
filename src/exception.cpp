/*
 * Copyright (C) 2020-2021 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iomanip>
#include "exception.hpp"
#include "gettext.hpp"

Osa::Exception::InvalidArg::InvalidArg(const std::string& msg)
    : msg(_("Invalid command-line argument") + (": " + msg))
{
}

const char *Osa::Exception::InvalidArg::what() const noexcept
{
    return msg.c_str();
}

const char *Osa::Exception::DaemonFail::what() const noexcept
{
    return _("Failed to daemonize the process");
}

Osa::Exception::FileOpenFail::FileOpenFail(const std::string& path, bool read, bool write)
    : msg(_("Failed to open file"))
{
    msg += " [";

    if (read)
        msg += 'r';
    if (write)
        msg += 'w';

    msg += "]: ";

    std::ostringstream ospath;
    ospath << std::quoted(path);
    msg += ospath.str();
}

const char *Osa::Exception::FileOpenFail::what() const noexcept
{
    return msg.c_str();
}

Osa::Exception::FileReadFail::FileReadFail(const std::string& path)
    : msg(_("Failed to read from file"))
{
    msg += ": ";

    std::ostringstream ospath;
    ospath << std::quoted(path);
    msg += ospath.str();
}

const char *Osa::Exception::FileReadFail::what() const noexcept
{
    return msg.c_str();
}

Osa::Exception::FileWriteFail::FileWriteFail(const std::string& path)
    : msg(_("Failed to write to file"))
{
    msg += ": ";

    std::ostringstream ospath;
    ospath << std::quoted(path);
    msg += ospath.str();
}

const char *Osa::Exception::FileWriteFail::what() const noexcept
{
    return msg.c_str();
}

Osa::Exception::FileCloseFail::FileCloseFail(const std::string& path)
    : msg(_("Failed to close file"))
{
    msg += ": ";

    std::ostringstream ospath;
    ospath << std::quoted(path);
    msg += ospath.str();
}
const char *Osa::Exception::FileCloseFail::what() const noexcept
{
    return msg.c_str();
}
