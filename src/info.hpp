/*
 * Copyright (C) 2020-2021 Nikola Hadžić
 *
 * This file is part of Osa.
 *
 * Osa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osa.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OSA_INFO_HPP
#define OSA_INFO_HPP
#include "gettext.hpp"

#define OSA_INFO_NAME   "Osa"
#define OSA_INFO_DESC   _("A keylogger")
#define OSA_INFO_VERS   "1.0"
#define OSA_INFO_LICE   "GPL3"

#endif
