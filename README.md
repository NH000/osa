# osa

## Description
Just another keylogger for Linux.
This program reads raw keycodes from the specified keyboard device file, converts them to human-readable names and writes them to a log file.

## Dependencies
For building and installing this program you'll need:

+ make
+ c++
+ msgfmt
+ sh
+ coreutils
+ sed (only for installing manual pages)
+ gettext

GCC and Clang are guaranteed to compile the project.

## Installation
Go into the project's root directory and run the following commands:

```
make all OPTIMIZE=1 # Build optimized executable.
make install        # Install executable and its assets.
make install-man    # Install manual pages.
```

For more options, run `make help`.

## Uninstallation
Enter the project's root directory and run the following commands:

```
make uninstall      # Uninstall executable and its assets.
make uninstall-man  # Uninstall manual pages.
```

Note that you should set installation make variables such as executable names, installation directory, etc. to values that were used during installation.

## Translations
Default language of this program (in effect when `C` or `POSIX` is set as locale) is US English.  
The following translations are available:

| **Language**       | **Translator**                                           | **For versions** |
|--------------------|----------------------------------------------------------|------------------|
| Serbian (Cyrillic) | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0              |
| Serbian (Latin)    | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0              |

### Translation process
This program is written to be easily translatable into multiple languages, and it achieves that through the use of [`gettext`](https://www.gnu.org/software/gettext/) library.
Translations are located in `po` directory. Files in that directory contain translations, each PO file corresponding to one locale.

To add a new or update existing translation, enter the project's `po` directory and run the following command:

```
make %.po  # Generate/update PO file; "%" should be replaced with a language code.
```

Afterwards, you can edit created/updated PO file (see [`gettext` manual](https://www.gnu.org/software/gettext/manual/gettext.html) for details),
translating the program that way.

You could also run `make messages.pot` to just generate the template file, but this will be done automatically by the previously described rule.

It would also be good to translate the manual page, you will find it in `man` project subdirectory.
